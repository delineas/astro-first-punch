export default function FaqItemReact({ title, children}) {
  return (
    <details className="faq-item">
      <summary className="faq-item__summary">{title}</summary>
      {children}
    </details>
  )
}